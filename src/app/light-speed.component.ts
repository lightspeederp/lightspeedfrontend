import { Component } from '@angular/core';

//inbuilt library path
import { AutoComplete } from 'primeng/primeng';
import { Routes , Router, ROUTER_DIRECTIVES , ROUTER_PROVIDERS } from '@angular/router';

//component path
import { HomePage } from './HomePage/HomePage';
import { Login } from './Login/Login';
// import { AddIngredient } from './Ingredient/AddIngredient/AddIngredient';
import { ViewIngredients } from './Ingredient/ViewIngredients/ViewIngredients';
import { InventoryPurchase } from './InventoryPurchase/InventoryPurchase';
import { MarketPlace } from './MarketPlace/MarketPlace';
import { SideMenu } from './Common/SideMenu/SideMenu';
import { ViewDetail } from './ViewDetail/ViewDetail';
import { ViewDetailTabComponent } from './view-detail-tab/view-detail-tab.component';



@Component({
  moduleId: module.id,
  selector: 'light-speed-app',
  templateUrl: 'light-speed.component.html',
  styleUrls: [ 'light-speed.component.css' ],
  directives: [ HomePage ,Login,AutoComplete,SideMenu, ROUTER_DIRECTIVES  ] ,
  
})

@Routes([
  { path:'/', component:HomePage },
  { path:'/login', component:Login },  
  { path:'/home', component:SideMenu },
  { path: 'view-ingredients-side', component: ViewIngredients },
  { path: '/view-detail-tab', component: ViewDetailTabComponent },
  // { path: '/add-ingredient', component: AddIngredient },  
  // { path: 'view-detail/:id', component: ViewDetail } 
])

export class LightSpeedAppComponent  {
  router: Router;
  constructor(_router: Router){       
       this.router = _router;
     //  this.router.navigateByUrl('/');
   }
}
