import { Component, OnInit } from '@angular/core';
import { Router , Routes , ROUTER_DIRECTIVES } from '@angular/router';
import { MarketPlace } from '../../MarketPlace/MarketPlace';
import { InventoryPurchase } from '../../InventoryPurchase/InventoryPurchase';
import { ViewIngredients } from '../../Ingredient/ViewIngredients/ViewIngredients';
import { ViewDetail } from '../../ViewDetail/ViewDetail';
import { AddIngredient } from '../../Ingredient/AddIngredient/AddIngredient';


 

@Component({
    moduleId: module.id,
    selector: 'side-menu',
    templateUrl: 'SideMenu.html',
    directives: [ ROUTER_DIRECTIVES , MarketPlace, InventoryPurchase,ViewIngredients  ]
    
})

@Routes([
    { path: '/', component: MarketPlace },
    { path: '/inventorypurchase', component: InventoryPurchase },
    { path: '/view-ingredients-side', component: ViewIngredients }, 
    { path: '/view-detail', component: ViewDetail },
    { path: '/add-ingredient', component: AddIngredient }
])
// /marketplace
export class SideMenu implements OnInit {
    private router:Router;
    constructor(private _router:Router) { 
        this.router=_router;
       // this.router.navigateByUrl('/main-page/marketplace');
        
    }

    ngOnInit() { }

}