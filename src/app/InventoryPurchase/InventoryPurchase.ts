import { Component, OnInit } from '@angular/core';
import { Tile } from '../Components/Tile/Tile';
import { Router , Routes , ROUTER_DIRECTIVES} from '@angular/router';
import { ViewIngredients } from '../Ingredient/ViewIngredients/ViewIngredients';


@Component({
    moduleId: module.id,
    selector: 'inventory-purchase',
    templateUrl: 'InventoryPurchase.html',
    directives:[ Tile , ROUTER_DIRECTIVES ]
    
})

//   @Routes([     
//        { path: '/main-page/view-ingredients', component: ViewIngredients }]
//   )  
    
export class InventoryPurchase implements OnInit {    
    router: Router;
    constructor(_router:Router) {
        this.router=_router;
     }
    
    ngOnInit() { }
    
    goToViewIngredients()
    {      
       //this.router.navigate['view-ingredients-side'];
       // this.router.navigate['/'];       
      // this.router.navigate[('/view-ingredients-side',id)];
       this.router.navigateByUrl('/home/view-ingredients-side');  
    }

}