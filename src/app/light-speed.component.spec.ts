import {
  beforeEachProviders,
  describe,
  expect,
  it,
  inject
} from '@angular/core/testing';
import { LightSpeedAppComponent } from '../app/light-speed.component';

beforeEachProviders(() => [LightSpeedAppComponent]);

describe('App: LightSpeed', () => {
  it('should create the app',
      inject([LightSpeedAppComponent], (app: LightSpeedAppComponent) => {
    expect(app).toBeTruthy();
  }));

  it('should have as title \'light-speed works!\'',
      inject([LightSpeedAppComponent], (app: LightSpeedAppComponent) => {
  //  expect(app.title).toEqual('light-speed works!');
  }));
});
