import {
  beforeEach,
  beforeEachProviders,
  describe,
  expect,
  it,
  inject,
} from '@angular/core/testing';
import { ComponentFixture, TestComponentBuilder } from '@angular/compiler/testing';
import { Component } from '@angular/core';
import { By } from '@angular/platform-browser';
import { ViewDetailTabComponent } from './view-detail-tab.component';

describe('Component: ViewDetailTab', () => {
  let builder: TestComponentBuilder;

  beforeEachProviders(() => [ViewDetailTabComponent]);
  beforeEach(inject([TestComponentBuilder], function (tcb: TestComponentBuilder) {
    builder = tcb;
  }));

  it('should inject the component', inject([ViewDetailTabComponent],
      (component: ViewDetailTabComponent) => {
    expect(component).toBeTruthy();
  }));

  it('should create the component', inject([], () => {
    return builder.createAsync(ViewDetailTabComponentTestController)
      .then((fixture: ComponentFixture<any>) => {
        let query = fixture.debugElement.query(By.directive(ViewDetailTabComponent));
        expect(query).toBeTruthy();
        expect(query.componentInstance).toBeTruthy();
      });
  }));
});

@Component({
  selector: 'test',
  template: `
    <app-view-detail-tab></app-view-detail-tab>
  `,
  directives: [ViewDetailTabComponent]
})
class ViewDetailTabComponentTestController {
}

