import { Component, OnInit } from '@angular/core';
import { RouteSegment, Router } from '@angular/router';
 
@Component({
    moduleId: module.id,
    selector: 'view-detail',
    templateUrl: 'ViewDetail.html'
})
export class ViewDetail implements OnInit {
    router:Router;
    constructor(_router:Router) {
        this.router=_router;
        //this.id = routeSegment.getParam('id');
     }

    private items: Object[];
    
    private products:Object[];
    
    private id:string;
    
    ngOnInit() {
        
        this.products=[{
            id:1,
            name:"Tomato" 
            },
            {
            id:2,
            name:"Chilli"
            },
            {
            id:3,
            name:"Garlic"
            }]
        this.items = [];
        this.items.push({label:'Categories'});
        this.items.push({label:'Sports'});
        this.items.push({label:'Football'});        
        this.items.push({label:'Lionel Messi', url: 'https://en.wikipedia.org/wiki/Lionel_Messi'});
    }
    
    goToAddIngredeint()
    {
        this.router.navigateByUrl('/home/add-ingredient');
    }

}