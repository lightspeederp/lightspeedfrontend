import { Component, OnInit, Input } from '@angular/core';
import { NgStyle } from '@angular/common';

@Component({
    moduleId: module.id,
    selector: 'tile',
    templateUrl: 'Tile.html'
})
export class Tile implements OnInit {
    
    @Input()  image:string;
    @Input()  color:string;
    @Input()  title:string;
    
    constructor() { }

    ngOnInit() { }

}