import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
    moduleId: module.id,
    selector: 'home-page',
    templateUrl: 'HomePage.html'
})
export class HomePage implements OnInit {
   router: Router;
   
   constructor(_router: Router){       
       this.router = _router;
   }
   
   //navigation event through button
   navigateLogin():void {
   this.router.navigateByUrl('/login');
   }

    ngOnInit() { }

}