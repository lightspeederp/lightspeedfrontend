import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    moduleId: module.id,
    selector: 'login',
    templateUrl: 'Login.html'
})
export class Login implements OnInit {
   router: Router;
   
   constructor(_router: Router){       
       this.router = _router;
   }
   
   //navigation event through button
   navigateNextPage():void {
   this.router.navigateByUrl('/home');
   }

   ngOnInit() { }

}