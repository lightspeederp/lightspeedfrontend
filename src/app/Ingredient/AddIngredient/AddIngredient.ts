import { Component, OnInit } from '@angular/core';
import { Breadcrumb, MenuItem, Button ,InputText , Dropdown} from 'primeng/primeng';
import { Router } from '@angular/router';


@Component({
    moduleId: module.id,
    selector: 'add-ingredient',
    templateUrl: 'AddIngredient.html',
    directives: [ Breadcrumb , Button, InputText , Dropdown] 
})
export class AddIngredient implements OnInit {
    router:Router;
    constructor(_router:Router) {
        
        this.router=_router;
     }

    private items: MenuItem[];
    
    ngOnInit() {
        this.items = [];
        this.items.push({label:'Categories'});
        this.items.push({label:'Sports'});
        this.items.push({label:'Football'});        
        this.items.push({label:'Lionel Messi', url: 'https://en.wikipedia.org/wiki/Lionel_Messi'});
    }
    goToViewIngredient()
    {
        
        this.router.navigateByUrl('/home/view-ingredients-side');
    }

}