import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    moduleId: module.id,
    selector: 'view-ingredients',
    templateUrl: 'ViewIngredients.html'
})
export class ViewIngredients implements OnInit {
    constructor(private router:Router) { }

    
    
    private items: Object[];
    
    private products:Object[];
    
    ngOnInit() {
        this.products=[{
            id:1,
            name:"Tomato" 
            },
            {
            id:2,
            name:"Chilli"
            },
            {
            id:3,
            name:"Garlic"
            }]
        this.items = [];
        this.items.push({label:'Categories'});
        this.items.push({label:'Sports'});
        this.items.push({label:'Football'});        
        this.items.push({label:'Lionel Messi', url: 'https://en.wikipedia.org/wiki/Lionel_Messi'});
    }
    
    viewDetailOfProduct(id)
    {
        
        this.router.navigateByUrl('/home/view-detail');
      //  this.router.navigate('/main-page/view-detail',id );
        
    }

}