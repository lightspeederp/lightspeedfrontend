export class LightSpeedPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('light-speed-app h1')).getText();
  }
}
