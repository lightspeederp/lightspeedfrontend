import { LightSpeedPage } from './app.po';

describe('light-speed App', function() {
  let page: LightSpeedPage;

  beforeEach(() => {
    page = new LightSpeedPage();
  })

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('light-speed works!');
  });
});
